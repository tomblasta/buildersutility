//
//  ProductsDetailsViewController.swift
//  BuildersUtility
//
//  Created by Thomas Black on 28/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class ProductsDetailsViewController: UITableViewController {
     //= productCell.
    //Properties
    
    //var rootController: TableViewController!
    var cell: UITableViewCell!
    var selectedCellName: String!
    //var selectedCellPath: Int!


    
    //Outlets
    
    
    //Actions
    
    @IBAction func unwindToViewController(segue: UIStoryboardSegue) {
        //let rootController = segue.sourceViewController as TableViewController
        self.dismissViewControllerAnimated(true, completion: nil)
        

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var names = [String](totalProducts.keys)
        productSectionTitles = names

        //totalProducts = [Product(name: "Wood", price: 19.99, type: "Wood"), Product(name: "Roofing", price: 29.99, type: "Roof"),Product(name: "Tiles", price: 9.99, type: "Tile")]
        
        
        //productSectionTitles = totalProducts.keys
        /*individualProduct.name = "Wood1"
        individualProduct.type.append("Woods")
        individualProduct.price = 19.99 //per meter^2
        
        totalProducts.append(individualProduct)
        
        individualProduct.name = "Tile1"
        individualProduct.type.append("Tiles")
        individualProduct.price = 25.00
        
        totalProducts.append(individualProduct)
        
        individualProduct.name = "Roofing1"
        individualProduct.type.append("Roofing")
        individualProduct.price = 9.99
        
        totalProducts.append(individualProduct)*/
        
        //var wood = ["Woods":["Wood1","Wood2","Wood3"]]
        /*var tiles = ["Tiles":["Tiles1","Tiles2","Tiles3"]]
        var roofing = ["Roofing":["Roofing1","Roofing2","Roofing2"]]
        
        totalProducts = [wood, tiles, roofing]
        
        var totalProducts = [
            "Tiles":[
                "Tiles1",
                "Tiles2",
                "Tiles3"
            ],
            "Woods":[
                "Wood1",
                "Wood2",
                "Wood3"
            ],
            "Roofing":[
                "Roofing1",
                "Roofing2",
                "Roofing2"
            ]
        ]*/


        /*for (key,i) in totalProducts {
            productSectionTitles.append(key)
        }*/
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return productSectionTitles.count
    }
    

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var returnHeader = productSectionTitles[section]

        return returnHeader
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        var find = totalProducts[productSectionTitles[section]]
        
        var returnNumRows = find?.count
        /*switch (section) {
        case 0:
            returnNum = 3
        case 1:
            returnNum = 3
        case 2:
            returnNum = 3
        default:
            returnNum = 3
        }*/
        return returnNumRows!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath) as UITableViewCell        //
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cellID")
        //var indexRow = indexPath.length as Int
        //var keyOf = productSectionTitles[indexPath.section]
        //var valueOf = totalProducts[keyOf]!
        var find = totalProducts[productSectionTitles[indexPath.section]]!
        
        selectedCellName = find[indexPath.row]
        
        cell.textLabel.text = find[indexPath.row]

        return cell
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //find the section
        var findInTotalProducts = totalProducts[productSectionTitles[indexPath.section]]!
        var findInPrices = prices[productSectionTitles[indexPath.section]]!

        pricesCarrier = findInPrices[indexPath.row]
        productLabelText = findInTotalProducts[indexPath.row]
        //dismiss the modal view
        self.dismissViewControllerAnimated(true, completion: nil)
        

    }
    /*func updateText() {
    
        //rootController.detailsLabel?.text = selectedCell
        //rootController.productLabel?.text = selectedCell
        rootController.transfer = selectedCell
        //rootController.productLabel?.text = "test"
        println(selectedCell)
        println(rootController.transfer)
        /3println(rootController.productLabel?.text)
    }*/
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    /*// MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }*/
    

}

/*Hi,
I have written an app in swift and in the part with the problem it has two views with there corresponding view controllers. What the aim of this part is for the user to click a certain cell on controller 1 which opens a modal view with a table view with many cells to select from. when the user taps one it should return to the controller 1 and update the details text on original cell.
The problem is that it won't transfer at all. here is the code:

this code is from the modal views controller. this is the code which gets the selected cell and updates a variable.

override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    var find = totalProducts[productSectionTitles[indexPath.section]]!
    //vc.updateText(find[indexPath.row])
    selectedCell = find[indexPath.row]
    //vc.updateText(selectedCell!)
    let rootController = TableViewController()
    println("\(selectedCell) from productController from selectedCell variable")
    rootController.text(selectedCell)
    self.dismissViewControllerAnimated(true, completion: nil)
    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
    //productCellLink.detailsLabel?.text = selectedCell
}



this code is from controller 1 where the modal view is launched from. this is called from

func text() {
    println("\(transfer) from rootController from transfer variable")
    detailsLabel?.text = transfer
    
}


So the variable 'transfer' works perfectly, it tranfers the data between the results between the controller but as soon as I try to assign it*/
