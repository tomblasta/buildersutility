//
//  Product.swift
//  BuildersUtility
//
//  Created by Thomas Black on 27/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    init(name: String, price: Double, type: String) {
        self.name = name
        self.price = price
        self.type = type
        //allTypes.append(type)
    }
    
    var name: String!
    var price: Double!
    var type: String!
    var allTypes: [String]!
}
