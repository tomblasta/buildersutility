//
//  TableViewController.swift
//  BuildersUtility
//
//  Created by Thomas Black on 28/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    //Properties
    var splashView: CBZSplashView!
    var recognizer: UITapGestureRecognizer!
    var productsController: ProductsDetailsViewController!
    var placeHolder: String!
    var resetCounter: Int!
    //Outlets
    @IBOutlet var amountTextField: UITextField! //Cell 2
    @IBOutlet var productLabel: UILabel! //Cell 3
    @IBOutlet var amountLabel: UILabel! //Cell 3
    @IBOutlet var costLabel: UILabel! //Cell 3
    @IBOutlet var rightDetail: UILabel!

    //Actions
    @IBAction func calculate() {

        
        
        if amountTextField.text != "" {
            amountLabel.text = amountTextField.text
        }
        var convert: Double? = (amountTextField.text as NSString).doubleValue as Double
        
        if (pricesCarrier == nil || amountLabel.text == "") {
            let alert = UIAlertController(title: "Error", message: "Enter data in Text fields", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            var result = (amountTextField.text as NSString).doubleValue as Double * pricesCarrier!
            costLabel.text = "$\(result)"

        }
        
        
        println(pricesCarrier)
        println(convert)
        println(costLabel.text)
        
        /*switch (pricesCarrier?, amountTextField?) == (nil, nil) {
            
        }*/
        

    }
    
    @IBAction func reset() {

        amountTextField.text = ""
        rightDetail.text = "Detail"
        productLabel.text = "Products"
        amountLabel.text = "Amount"
        costLabel.text = "Cost"
        println("called")
        
        
    }
    @IBAction func addMultipleValues() {
        
    }
    
    
    @IBAction func viewTapped(sender: AnyObject) {

        amountTextField.resignFirstResponder()
    }

    /*func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        var cancelsTouchesInView: Bool = false
        return true
    }*/
   //
    

    
    override func viewWillAppear(animated: Bool) {
        /*println(placeHolderRightDetail)
        println(productLabelText)
        println(rightDetail.text)*/
        placeHolder = productLabelText
        if placeHolder != nil {
            rightDetail.text = placeHolder
            productLabel.text = placeHolder
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amountTextField?.text = nil
        resetCounter = 0
        //splashView.animationDuration = 1.4
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        recognizer = UITapGestureRecognizer(target: self, action:Selector("viewTapped:"))

        recognizer.delegate = self

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        view.addGestureRecognizer(recognizer)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        view.removeGestureRecognizer(recognizer)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 5
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }

    
    /*override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //cell = tableView.dequeueReusableCellWithIdentifier("cellID", forIndexPath: indexPath) as UITableViewCell
        var cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cellID")

        
        return cell
        
    }*/

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
