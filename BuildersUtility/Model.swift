//
//  Model.swift
//  BuildersUtility
//
//  Created by Thomas Black on 26/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import Foundation


//transfer variables
var productLabelText: String!
var pricesCarrier: Double?

//global vars
var productSectionTitles: [String]!

var totalProducts = [
    "Tiles":[
        "Tiles1",
        "Tiles2",
        "Tiles3",
        "Tiles4",
        "Tiles5"
    ],
    "Woods":[
        "Wood1",
        "Wood2",
        "Wood3",
        "Wood4",
        "Wood5",
        "Wood6",
        "Wood7",
        "Wood8"
    ],
    "Roofing":[
        "Roofing1",
        "Roofing2",
        "Roofing3",
        "Roofing4",
        "Roofing5",
        "Roofing6"
    ],
    "Cabnetry": [
        "Cabnet1",
        "Cabnet2",
        "Cabnet3",
        "Cabnet4"
    ]
]

var prices: [String: [Double]] = [
    "Tiles":[
        10.00,
        15.00,
        17.00,
        23.00,
        62.00
    ],
    "Woods":[
        12.5,
        15.75,
        2.3,
        3.2,
        9.99,
        10.00,
        45.6,
        12.3
    ],
    "Roofing":[
        12.00,
        13.00,
        14.00,
        15.00,
        16.00,
        17.00
    ],
    "Cabnetry": [
        1,
        2,
        3,
        4
    ]
]

/*

add new products - view controller needed - modal style

select product - view controller - push





*/
