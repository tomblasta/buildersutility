//
//  ViewController.swift
//  BuildersUtility
//
//  Created by Thomas Black on 26/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //productPicker.dataSource = self
        
        //productPicker.delegate = self
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*@IBOutlet var productLabel: UILabel! //set as equivalent to "\(productPicker.text)"
    @IBOutlet var byAmountLabel: UILabel! //set as equivalent to "By \(amountTextField.text)"
    @IBOutlet var productPicker: UIPickerView!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var results: UILabel!
    
    @IBAction func calculate() {

        /* var pickerDataResults = pickerDataValues[productPicker.selectedRowInComponent(0)]
        var amountDataResults = (amountTextField.text as NSString).doubleValue */
        
        var resultsCalculated = price(pickerDataValues[productPicker.selectedRowInComponent(0)], (amountTextField.text as NSString).doubleValue)
        
        results.text = "\(resultsCalculated)"
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        amountTextField.resignFirstResponder()
        byAmountLabel.text = "by \(amountTextField.text)"
    }
    

    var pickerDataValues = [10.00, 20.00, 30.00, 40.00, 50.00, 60.00]
    var pickerData = ["Product1","Product2","Product3","Product4","Product5","Product6"]
    
//Picker View Code
    
    //Delegate required methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1 //hardcode this to one since only 1 spinner wheel (component) otherwise would be return. component is an actual 'spinner wheel'
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count //this is counting the number of rows in the actual component - if was array within array code would have read pickerData[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerData[row]
    }
    
    //non required method
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var selectedProduct = pickerData[productPicker.selectedRowInComponent(0)]
        productLabel.text = "\(selectedProduct)" //updates product label
    }*/
    

}

